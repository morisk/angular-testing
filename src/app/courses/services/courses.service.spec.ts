import { CoursesService } from './courses.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { COURSES, findLessonsForCourse } from '../../../../server/db-data';
import { Course } from '../model/course';

describe('CoursesService', () => {

    let coursesService: CoursesService,
        httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ CoursesService ]
        });

        coursesService = TestBed.get(CoursesService);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should retrieve all courses', () => {
        coursesService.findAllCourses()
            .subscribe( courses => {
                expect(courses).toBeTruthy();
                expect(courses.length).toBe(12);

                const course = courses.find(c => c.id === 12);
                expect(course.titles.description).toEqual('Angular Testing Course');
            }
        );

        const req = httpTestingController.expectOne('/api/courses');
        expect(req.request.method).toEqual('GET');
        req.flush({ payload: Object.values(COURSES) } );
    });

    it('should retrieve a course by id', () => {
        coursesService.findCourseById(12)
            .subscribe( course => {

                expect(course).toBeTruthy();
                expect(course.id).toBe(12);
            }
        );

        const req = httpTestingController.expectOne('/api/courses/12');
        expect(req.request.method).toEqual('GET');
        req.flush( COURSES[12] );
    });

    it('should update a course changes', () => {
        const newDesc: Partial<Course> = { titles: { description: 'Testing course 2'} };

        coursesService.saveCourse(12, newDesc)
            .subscribe( course => {
                console.log(course);
                expect(course.id).toBe(12);
            });

        const req = httpTestingController.expectOne('/api/courses/12');
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body.titles).toEqual(newDesc.titles);
        req.flush({ 
            ...COURSES[12],
            ...newDesc
        });
    });

    it('should fail on server error', () => {
        const newDesc: Partial<Course> = { titles: { description: 'Testing course 2'} };

        coursesService.saveCourse(12, newDesc).subscribe(
            () => fail('It sholud have had failed on server error'),
            (error) => {
                expect(error.status).toBe(500);
            }
        );

        const req = httpTestingController.expectOne('/api/courses/12');
        expect(req.request.method).toEqual('PUT');

        req.flush('Something failed',{ 
            status: 500, statusText: 'Internal Server Error'
        });
    });

    it('should a list of lessons for a given course', () => {
        coursesService.findLessons(12).subscribe(
            lessons => {
                expect(lessons).toBeTruthy();
                expect(lessons.length).toBe(3);
            }
        );

        const req = httpTestingController.expectOne( (req) => req.url === '/api/lessons');
        expect(req.request.method).toEqual('GET');
        expect(req.request.params.get('courseId')).toEqual('12');
        expect(req.request.params.get('filter')).toEqual('');
        expect(req.request.params.get('sortOrder')).toEqual('asc');
        expect(req.request.params.get('pageNumber')).toEqual('0');
        expect(req.request.params.get('pageSize')).toEqual('3');

        req.flush({ payload: findLessonsForCourse(12).slice(0,3) });
    });

    afterEach(() => {
        httpTestingController.verify();
    });

});